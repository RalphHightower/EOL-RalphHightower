# Songs Chosen for the Day

## Slide show: <br>
- [Moody Blues: Your Wildest Dream](https://youtu.be/kmmPFrkuPq0) <br>

## Songs for Service:<br>
- [Byrds: Turn, Turn, Turn](https://youtu.be/eiprqeaydik)<br>
- [Fleetwood Mac: Landslide](https://youtu.be/WM7-PYtXtJM)<br>
