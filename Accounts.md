## Accounts

| word | definition |
|------|------------|
| 2FA | Two Factor Authentication |
| Subscription | Paid Subscription |
| Action | What to do |

| Account <image width='10%'> |   User Name <image width='10%'> | 2FA <image width='10%'>  | Subscription <image width='10%'> | Action <img width='50%'> |
|-------------|------------------------|-----------|------------------|-----------------------------------------------|
| LMC MyChart | RalphHightower | Yes[^1][^2] | No | Don't know |
| LinkedIn   | RalphHightower         | Yes[^1] | Yes | [Memorialize Account](https://www.linkedin.com/help/linkedin/ask/TS-RDMLP?lang=en)[^3]  |
| GitHub Pro (Team) | RalphHightower | Yes[^4] | Yes | [Downgrading your GitHub subscription](https://docs.github.com/en/billing/managing-billing-for-your-github-account/downgrading-your-github-subscription)[^5] | 
| [MagPI — Official Raspberry Magazine](https://magpi.raspberrypi.com/) | ralph.hightower@gmail.com | No | Yes | Cancel |
| [Ebay](https://www.ebay.com) | rhig6871 | No | No | Close |
| [https://gocomics.com](https://gocomics.com) | ralph.hightower@gmail.com | No | Yes | Cancel |
| [https://comicskingdom.com/](https://comicskingdom.com) | ralph.hightower@gmail.com | No | Yes | Cancel |
| [https://stanza.co](https://stanza.co) | ralph.hightower@gmail.com | No | Yes | Cancel |
| [Terminus \(https://termius.com)](https://termius.com/) | ralph.hightower@gmail.com | No | Yes | Cancel |
| [Uber \(https://www.uber.com/\)](https://www.uber.com/) | ralph.hightower@gmail.com | No | Yes | Cancel |
 
[^1]: Text message to Ralph's cellphone. 
[^2]: Biometric (right indexfinger print) 
[^3]: [https://www.linkedin.com/help/linkedin/ask/TS-RDMLP?lang=en](https://www.linkedin.com/help/linkedin/ask/TS-RDMLP?lang=en)
[^4]: Need to run GitHub on Ralph Hightower's personal cellphone.
[^5]: [https://docs.github.com/en/billing/managing-billing-for-your-github-account/downgrading-your-github-subscription](https://docs.github.com/en/billing/managing-billing-for-your-github-account/downgrading-your-github-subscription)
