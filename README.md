# EOL-RalphHightower
End Of Life information for survivor and/or executors, probate. I have a Gary Larson, "Far Side", sense of humor, so the term, EOL, commonly used by Microsoft for End Of Life fits.

I got the idea from [Chrissy LeMaire — @potatoqualitee](https://github.com/potatoqualitee) [eol-dr](https://github.com/potatoqualitee/eol-dr) during a Women In Technology webinar on Mental Health Awareness Day on April 8, 2021.
- [Obituary](Obituary.md)
- [Songs for Service](SongsForService.md)
- [Assets Endowment (Established with attorney)](Assets.md)
- [Beagles](Beagles.md)
- [Beagle Identification Photos](BeagleIdentificationPhotos.md)
- [Accounts w/o passwords](Accounts.md)
- [Equipment Disposition](EquipmentDisposition.md)
- [Computer Settings](ComputerSettings.md)
- [Network Settings](NetworkSettings.md)

## Paula: If you have not done so yet, file with the Lexington County Assessor (first floor) for Homestead Tax Exemption, which removes $50,000 off the assessed value of our home for real property taxes.
[![Ralph Hightower's GitHub activity graph](https://activity-graph.herokuapp.com/graph?username=RalphHightower&theme=react&days=365)](https://github.com/RalphHightower/EOL-RalphHightower/github-readme-activity-graph)
